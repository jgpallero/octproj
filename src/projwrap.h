/* -*- coding: utf-8 -*- */
/**
\defgroup octproj octPROJ module
@{
\file projwrap.h
\brief Function declaration for PROJ wrapper.
\author José Luis García Pallero, jgpallero@gmail.com
\date 05-12-2009
\date 22-02-2020
\section License License
This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _PROJWRAP_H_
#define _PROJWRAP_H_
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<proj.h>
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def PROJ_ERR_NOT_INV_PROJ
\brief Error identifier from PROJ. Does not exist inverse step for a defined
       projection.
\date 13-04-2012: Constant creation.
*/
#define PROJ_ERR_NOT_INV_PROJ -17
/******************************************************************************/
/******************************************************************************/
/**
\def PROJWRAP_ERR_NOT_INV_PROJ
\brief Error identifier. Does not exist inverse step for a defined projection.
\date 12-12-2009: Constant creation.
*/
#define PROJWRAP_ERR_NOT_INV_PROJ 10001
/******************************************************************************/
/******************************************************************************/
/**
\def PROJWRAP_ERR_NOT_PROJECTION
\brief Error identifier. The indicated projection does not exist.
\date 22-02-2020: Constant creation.
*/
#define PROJWRAP_ERR_NOT_PROJECTION 10002
/******************************************************************************/
/******************************************************************************/
/**
\brief Wrapper for forward step.
\param[in,out] u Array containing the geodetic longitude. On output, this
               argument contains the X projected coordinates. Angular
               coordinates are radians, and linear, meters. But it can be
               changed in \em params, so the data in vector must be congruent.
\param[in,out] v Array containing the geodetic latitude. On output, this
               argument contains the Y projected coordinates. Angular
               coordinates are radians, and linear, meters. But it can be
               changed in \em params, so the data in vector must be congruent.
\param[in] nElem Number of elements in \em u and \em v arrays.
\param[in] incElem Number of positions between each element in the arrays \em u
           and \em v (must be a positive number).
\param[in] params List containing the parameters of the projection, in PROJ
           format with \p + signs (see https://proj.org/usage/index.html). In
           this function, \b *ONLY* the \p + format is permitted.
\param[out] errorText If an error occurs, explanation text about the error.
\param[out] projectionError Two posibilities:
            - < 0: All projected points are OK.
            - Otherwise: Some points have been projected with errors. This value
              only have sense if the returning error code of the function is not
              0. The value is the position of the first missprojected point.
\return Error code. Three posibilities:
        - 0: No error.
        - #PROJWRAP_ERR_NOT_PROJECTION: The definition in \em params is not
          cartographic projection.
        - Otherwise: Error code of PROJ, see documentation.
\note If projection errors occur, the positions of the erroneous points in \em u
      and \em v arrays store the value HUGE_VAL (constant from math.h).
\date 12-12-2009: Function creation.
\date 13-05-2011: Change the name of \em lon and \em lat variables to \em u and
      \em v, add the new variable \em incElem and use internally \em projLP and
      \em projXY structures instead only \em projXY (actually, both are
      synonyms).
\date 22-02-2020: Upgrade to PROJ >= 6.
*/
int proj_fwd(double* u,
             double* v,
             const size_t nElem,
             const size_t incElem,
             const char params[],
             char errorText[],
             int* projectionError);
/******************************************************************************/
/******************************************************************************/
/**
\brief Wrapper for inverse step.
\param[in,out] u Array containing the X projected coordinates. On output, this
               argument contains the geodetic longitude. Angular coordinates are
               radians, and linear, meters. But it can be changed in \em params,
               so the data in vector must be congruent.
\param[in,out] v Array containing the Y projected coordinates. On output, this
               argument contains the geodetic latitude. Angular coordinates are
               radians, and linear, meters. But it can be changed in \em params,
               so the data in vector must be congruent.
\param[in] nElem Number of elements in \em u and \em v arrays.
\param[in] incElem Number of positions between each element in the arrays \em u
           and \em v (must be a positive number).
\param[in] params List containing the parameters of the projection, in PROJ
           format with \p + signs (see https://proj.org/usage/index.html). In
           this function, \b *ONLY* the \p + format is permitted.
\param[out] errorText If an error occurs, explanation text about the error.
\param[out] projectionError Two posibilities:
            - < 0: All projected points are OK.
            - Otherwise: Some points have been projected with errors. This value
              only have sense if the returning error code of the function is not
              0 nor #PROJWRAP_ERR_NOT_INV_PROJ. The value is the position of the
              first missprojected point.
\return Error code. Four posibilities:
        - 0: No error.
        - #PROJWRAP_ERR_NOT_INV_PROJ: Do not exist inverse step for the defined
          projection.
        - #PROJWRAP_ERR_NOT_PROJECTION: The definition in \em params is not
          cartographic projection.
        - Otherwise: Error code of PROJ, see documentation.
\note If projection errors occur, the positions of the erroneous points in
      \em u and \em v arrays store the value HUGE_VAL (constant from math.h).
\date 12-12-2009: Function creation.
\date 13-05-2011: Change the name of \em x and \em y variables to \em u and
      \em v, add the new variable \em incElem and use internally \em projLP and
      \em projXY stryctures instead only \em projXY (actually, both are
      synonyms).
\date 22-02-2020: Upgrade to PROJ >= 6.
*/
int proj_inv(double* u,
             double* v,
             const size_t nElem,
             const size_t incElem,
             const char params[],
             char errorText[],
             int* projectionError);
/******************************************************************************/
/******************************************************************************/
/**
\brief Wrapper for CRS transformations.
\param[in,out] u Array containing the first coordinate. On output, this argument
               contains the transformed coordinates. This array can store two
               types of values:
               - If the system (start or end) is geodetic, this array contains
                 geodetic longitude.
               - Otherwise this array contains \em X euclidean coordinates.
                 Angular coordinates are degrees, and linear, meters. But it can
                 be changed in \em params, so the data in vector must be
                 congruent.
\param[in,out] v Array containing the second coordinate. On output, this
               argument contains the transformed coordinates. This array can
               store two types of values:
               - If the system (start or end) is geodetic, this array contains
                 geodetic latitude.
               - Otherwise this array contains \em Y euclidean coordinates.
                 Angular coordinates are degrees, and linear, meters. But it can
                 be changed in \em params, so the data in vector must be
                 congruent.
\param[in,out] z Array containing heights. This argument can be \p NULL. In that
               case it is not used. Linear coordinates are meters, but it can be
               changed in \em params, so the data in vector must be congruent.
\param[in,out] t Array containing times for dynamical transformations. This
               argument can be \p NULL. In that case it is not used.
\param[in] nElem Number of elements in \em u, \em v, \em z, and \em t arrays.
\param[in] incElem Number of positions between each element in the arrays \em u,
           \em v, \em z, and \em t (must be a positive number).
\param[in] paramsStart List containing the parameters of the start system, in
           PROJ format. All formats are permitted (\p + format or \p EPSG code).
\param[in] paramsEnd List containing the parameters of the end system, in PROJ
           format. All formats are permitted (\p + format or \p EPSG code).
\param[out] errorText If an error occurs, explanation text about the error. This
            argument must be assigned enough memory.
\param[out] transformationError Two posibilities:
            - < 0: All projected points are OK.
            - Otherwise: Some points have been transformed with errors. This
              value only have sense if the returning error code of the function
              is not 0. The value is the position of the first misstransformed
              point.
\return Error code. Two posibilities:
        - 0: No error.
        - Otherwise: Error code of PROJ, see documentation.
\date 05-12-2009: Function creation.
\date 13-05-2011: Change the name of \em x and \em y variables to \em u and
      \em v and add the new variable \em incElem.
\date 22-02-2020: Upgrade to PROJ >= 6.
*/
int proj_transform(double* u,
                   double* v,
                   double* z,
                   double* t,
                   const size_t nElem,
                   const size_t incElem,
                   const char paramsStart[],
                   const char paramsEnd[],
                   char errorText[],
                   int* transformationError);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
